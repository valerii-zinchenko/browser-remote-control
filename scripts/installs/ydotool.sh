#!/usr/bin/env bash

set -x

requiredPackages=(ydotool wl-clipboard)
packagesToInstall=(ydotool wl-clipboard)

# check if package is installed
#for pkg in "${requiredPackages[@]}"; do
#	result=(`dpkg-query -W -f '${binary:Package} ${Version}\n' $pkg`)
#	if [[ -z $result || ${#result[@]} -lt 2 ]]; then
#		packagesToInstall+=($pkg)
#	fi
#done

# install missed packages in application space
if ((${#packagesToInstall[@]} > 0)); then
	mkdir -p .venv/tmp
	cd .venv/tmp

	# download archives
	for pkg in "${packagesToInstall[@]}"; do
		if [ $pkg = ydotool ]; then
			wget https://github.com/ReimuNotMoe/ydotool/archive/refs/tags/v1.0.4.tar.gz -O ydotool-1.0.4.tar.gz
		else
			apt-get download $pkg
		fi
	done

	# install downloaded packges
	for pkg in ./*; do
		if [[ "$pkg" =~ ydotool.* ]]; then
			tar -xf ydotool-1.0.4.tar.gz
			cd ydotool-1.0.4
			mkdir build
			cd build
			cmake ../
			make -j `nproc`
			cp ydotool ydotoold ../../../usr/bin
			cd ../../
			rm ydotool-1.0.4.tar.gz
			rm -r ydotool-1.0.4
		else
			dpkg -x ./"$pkg" ../
			rm ./"$pkg"
		fi
	done

	cd ../
	rmdir ./tmp
	cd ../
fi
