#!/usr/bin/env bash


git fetch --tags

_latestTag=$(git describe --tags)
_currentTag=$(cat ./.venv/version)1

if [ "$_currentTag" = "$_latestTag" ]; then
    exit 0
fi

git checkout $_latestTag

./install.sh

echo $_latestTag > ./.venv/version
