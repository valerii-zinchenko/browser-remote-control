export default {
  "keyboard": [
    ["esc",     "",     "enter"      ],
    ["delete", "space",   "backspace"],
    ["ctrl+c",  "",     "ctrl+v"     ],
    [                                ],
    ["pageup", "up",   "pagedown"    ],
    ["left",   "down", "right"       ],
    ["home",   "",     "end"         ],
    ["",       "tab",  ""            ],
  ],
  "media": [
    ["back",           "",         "forward"     ],
    ["homepage",       "www",      "refresh"     ],
    [                                            ],
    ["volumedown",     "mute",     "volumeup"    ],
    [                                            ],
    ["brightnessdown", "",         "brightnessup"],
    [                                            ],
    ["",               "leftmeta", "alt+f4"      ],
    [                                            ],
    ["",               "power",    ""            ],
  ]
}
