import http from 'http';
import fs from 'fs';
import path from 'path';
import url from 'url';
import os from 'os';

import mime from 'mime-types';
import QRCode from 'qrcode';

import fileProcessors from './fileProcessor.mjs'


const host = '0.0.0.0';
const port = process.env.PC_RC_PORT || 80;


const CUSTOM_MIME = {
	'.scss': mime.lookup('css'),
};

function readFile(path) {
	try {
		return fs.readFileSync(path, {encoding: 'utf8'});
	} catch (err) {
		return null;
	}
}

export default function createServer(servingFolder = './src/') {
	return http.createServer((rq, rs) => {
		const resource = url.parse(rq.url).pathname;
		const filename = resource === '/' ? 'index.html' : resource.slice(1);
		const ext = path.extname(filename);

		const mimeType = CUSTOM_MIME[ext] || mime.lookup(ext);
		if (!mimeType) {
			console.info(`[${new Date().toISOString()}] GET ${filename} (E400: Unknown file extensions "${ext}")`);
			rs.writeHead(400);
			rs.end();
			return;
		}

		const localPath = path.join(servingFolder + filename);
		fs.exists(localPath, exists => {
			if (!exists) {
				rs.writeHead(404);
				rs.end();
				return;
			}

			let contents = readFile(localPath);
			if (contents === null) {
				rs.writeHead(500);
				rs.end();
				return;
			}

			const fileProcessor = fileProcessors[ext];
			if (fileProcessor) {
				const filePath = path.join(filename);
				try {
					contents = fileProcessor(contents, filePath);
				} catch (err) {
					console.error(filePath, err);
					rs.writeHead(500);
					rs.end();
					return;
				}
			}

			rs.writeHead(200, {
				'Content-Type': mimeType,
				'Content-Length': contents.length
			});
			rs.end(contents);
		});
	}).listen(port, host);
}

const networkInterfaces = os.networkInterfaces();
const urls = [
    `http://${os.hostname()}.local:${port}`
];
for (const key in networkInterfaces) {
	networkInterfaces[key].forEach(item => {
		if (item.internal || item.family !== 'IPv4') {
			return;
		}

		urls.push(`http://${item.address}:${port}`);
	});
}


console.info(`Server is running at http://${host}:${port}`);
console.info(`Client URLs:
\t${urls.join('\n\t')}`);

(async () => {
	const urlsQR = {};
	await Promise.all(urls.map(
		(url, index) => new Promise(res =>
			QRCode.toDataURL(url, {
				margin: 2,
				scale: index === 0 ? 10 : 4,
			}, (error, dataUrl) => {
				if (dataUrl) {
					urlsQR[url] = dataUrl;
                }

                res();
            })
        )
    ));
    fs.writeFile('./src/urls.json', JSON.stringify(urlsQR), err => err && console.warn(err));
})();
