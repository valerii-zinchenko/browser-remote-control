import {exec, execSync} from 'node:child_process';

import {
    keyboard,
    mouseButton,
    mouseButtonState,
} from './keyMap.mjs';


// Mouse
// --------------------
async function touchmove(dx, dy) {
    await run('mousemove', '-x', dx, '-y', dy);
}
async function click(count = 1, button='left', btnState = 'click') {
    const mouseBtn = mouseButton[button];
    const state = mouseButtonState[btnState];
    if (!button) {
        return;
    }

	await run('click', '--repeat', count, (mouseBtn + state).toString(16));
}
async function scroll(dx, dy) {
	await run('mousemove', '--wheel', '-x', dx, '-y', dy);
}
// --------------------

// Keyboard
// --------------------
async function keyboardType(msg) {
    execSync(`wl-copy "${msg}"`, { stdio: 'ignore' });
	await hotkey(['leftctrl', 'v']);
}
async function keyboardPress(...keys) {
	await hotkey(keys);
}
async function hotkey(keys) {
    const codes = keys.map(k => keyboard[k.toLowerCase()]);

    await run('key', `${codes.join(':1 ')}:1 ${codes.reverse().join(':0 ')}:0`);
}
// --------------------

function run(cmd, ...args) {
	return new Promise((resolve, reject) => {
        exec(`ydotool ${cmd} ${args.join(' ')}\n`, (error, stdout, stderr) => {
            if (stdout) {
                console.log(`ydotool log: ${stdout}\n`);
            }
            if (stderr) {
                console.error(`ydotool error: ${stderr}\n`);
            }

            if (error) {
                reject(error);
                return;
            }

            resolve();
        });
	});
}

export const requestAPI = {
    touchmove,
    click,
    scroll,

    keyboardType,
    keyboardPress,
    hotkey,
};
