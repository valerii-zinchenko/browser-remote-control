import {argv} from 'process';

import httpServer from './http-server.mjs';
import wsServer from './web-socket-server.mjs';

import * as controller from './controllers/ydotool/index.mjs';

const server = httpServer(argv[2]);
const ws = wsServer({
	server,
	clientTracking: true,
}, controller);
