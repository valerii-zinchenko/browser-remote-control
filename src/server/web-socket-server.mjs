import {WebSocketServer} from 'ws';

import {encode} from '../shared/wsData.mjs';
import wsMessageProcessor from '../shared/wsMessageProcessor.mjs';


export default async (options, controller) => {
	const ws = new WebSocketServer(options);

	ws.on('connection', connection => {
		const id = Date.now();

		connection.on('message', wsMessageProcessor.bind(null, connection, {
			request: controller.requestAPI,
			log(msg) {
				console.info(`[${new Date().toISOString()}] ${id} WS log: ${msg}`);
			},
			error(msg) {
				console.error(`[${new Date().toISOString()}] ${id} WS error: ${msg}`);
			}
		}));
		connection.on('close', onConnectionClose.bind(connection, id, controller));

		if (controller.setupConnection)
			controller.setupConnection(connection, id);

		console.info(`[${new Date().toISOString()}] New client just connected: ${id}`);
	});

	if (controller.init)
		await controller.init(ws);

	return ws;
}

async function onConnectionClose(id, controller) {
	console.info(`[${new Date().toISOString()}] Client disconnected: ${id}`);

	if (controller.close)
		await controller.close();
}
