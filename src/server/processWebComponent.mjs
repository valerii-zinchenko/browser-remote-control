import path from 'path';

import mime from 'mime-types';
import { parse } from 'node-html-parser';

import {scss} from './processStyle.mjs';


export default function processComponent(content, filePath) {
	const result = parse(content);

	result.querySelectorAll('style').forEach(item => {
		item.innerHTML = scss(item.innerHTML);
	});

	const dir = path.dirname(filePath);
	result.querySelectorAll('html-import[src]').forEach(item => {
		const src = item.getAttribute('src');
		const alignedDir = path.join(dir, src);
		item.setAttribute('src', alignedDir);
	});

	const htmlStr = result.toString();
	return htmlStr;
}
