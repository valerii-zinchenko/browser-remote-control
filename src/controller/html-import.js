const loadingPromises = new Map();

class HTMLImportElement extends HTMLElement {
  static observedAttributes = ['src'];

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'src':
        this.changeModuleSrc(oldValue, newValue);
        break;
    }
  }

  async changeModuleSrc(oldValue, newValue) {
    const componentBuilder = await load(newValue);
    if (!componentBuilder) {
      return;
    }

    const component = componentBuilder(this.attributes, this.children);

    this.replaceWith(component);
  }
}
customElements.define('html-import', HTMLImportElement);


async function load(src) {
  let loadingPromise = loadingPromises.get(src);

  if (!loadingPromise) {
    loadingPromise = loading(src);
    loadingPromises.set(src, loadingPromise);
  }

  return loadingPromise;
}

async function loading(src) {
  let template = document.getElementById(src);
  if (template) {
    return processTemplate(template);
  }

  const response = await fetch(src);
  if (response.ok === false) {
    return;
  }

  const html = await response.text();

  template = document.createElement('template');
  template.setAttribute('id', src)
  template.innerHTML = html;
  // not sure if it is really needed
  // document.body.append(template);

  return processTemplate(template, src);
}

async function processTemplate(template, src) {
  const styleElements = Array.from(template.content.querySelectorAll('style'));

  const scripts = Array.from(template.content.querySelectorAll('script'));
  let hasComponentScript = false;
  let extendsOption;
  for (const item of scripts) {
    const script = document.createElement('script');

    const scriptSrc = item.getAttribute('src');
    const scriptType = item.getAttribute('type') || '';
    if (scriptSrc) {
      script.setAttribute('src', scriptSrc);
      script.setAttribute('type', scriptType);
      // for some unknown reasons clonning is not working
      //script = item.cloneNode();
      //script = item;
    } else {
      script.setAttribute('type', scriptType || 'module');
      script.innerHTML = item.innerHTML + `
document.dispatchEvent(new CustomEvent('${src}', {detail: {Component}}));
${HTMLImportElement.sourceMap ? `//# sourceURL=${src}.mjs` : ''}`;
      hasComponentScript = true;
      extendsOption = item.getAttribute('extends');
    }

    document.head.append(script);

    item.remove();
  }

  const [
    styles,
    WebComponentClass = EmptyWebComponent,
  ] = await Promise.all([
    Promise.all(styleElements.map((style) => {
      style.remove();
      if (HTMLImportElement.sourceMap) {
        style.innerHTML += `
/*# sourceURL=${[location.origin, src].join('/')}.css */`;
      }
      return new CSSStyleSheet().replace(style.innerHTML);
    })),
    hasComponentScript
      ? new Promise((resolve) => {
        document.addEventListener(src, ({detail: {Component}}) => {
          resolve(Component);
        });
      })
      : undefined,
  ]);

  const tag = 'app-' + src.toLowerCase()
        .replace(/\//g, '-')
        .replace('.html', '');
  customElements.define(
    tag,
    class extends WebComponentClass {
      constructor() {
        super();

        const clone = template.content.cloneNode(true);
        this.attachShadow({mode: 'open'})
        this.shadowRoot.adoptedStyleSheets = styles;
        this.shadowRoot.append(...clone.children);

        this.shadowRootAddedCallback?.();
      }
    },
    extendsOption ? {extends: extendsOption} : undefined
  );

  return (attributes, children) => {
    let component;
    if (extendsOption) {
      component = document.createElement(extendsOption, {is: tag});
      // the next line does nothing in genera, but it is just an indication for convenience
      component.setAttribute('is', tag);
    } else {
      component = document.createElement(tag);
    }

    for (const {name, value} of attributes) {
      if (name === 'src')
        continue;

      component.setAttribute(name, value);
    }

    component.append(...children);

    return component;
  }
}

class EmptyWebComponent extends HTMLElement {}
