 import fireEvent from './fireEvent.mjs';


 export default function press(...keys) {
     fireEvent('ws-request', {
         action: 'keyboardPress',
         args: keys
     });
 }
