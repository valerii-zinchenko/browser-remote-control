import {encode, decode} from '../shared/wsData.mjs';

function send(ws, obj) {
	ws.send(encode(obj));
}

function sendError(ws, msg) {
	console.info(`[${new Date().toISOString()}] WS error: ${msg}`);
	send(ws, {
		type: 'error',
		args: [msg]
	});
}

export default async function wsMessageProcessor(ws, typeActionProcessorMap, data) {
	//console.info(`[${new Date().toISOString()}] WS input: ${data}`);

	const message = decode(data);
	const {
		type,
		action,
		args = []
	} = message;

	let fn = typeActionProcessorMap[type];
	if (!fn) {
		sendError(ws, `Message type "${type}" is unknown`);
		return;
	}

	if (!(fn instanceof Function)) {
		fn = fn[action];
	}

	if (!(fn instanceof Function)) {
		//sendError(ws, `Action "${action}" for a message type "${type}" is unknown`);
		return;
	}

	let result;
	try {
		result = await fn.apply(ws, args);
	} catch (err) {
		console.error(err);
		sendError(ws, err.message);
	}

	if (!result) {
		return;
	}

	send(ws, {
		type: 'response',
		action,
		result
	});
}
