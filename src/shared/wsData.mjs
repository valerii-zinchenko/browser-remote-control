export function encode(data) {
	return JSON.stringify(data);
}

export function decode(data) {
	return JSON.parse(data);
}
